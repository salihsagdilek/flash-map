/**
 * Created by salih.sagdilek on 9.2.2015.
 */
module.exports = function(grunt){
    grunt.config('watch',{
        js: {
            options : {
                livereload:true
            },
            files : 'src/**/*.js',
            tasks : ['uglify']
        }
    });
    grunt.loadNpmTasks('grunt-contrib-watch');
};
