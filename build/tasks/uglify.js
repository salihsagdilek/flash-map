/**
 * Created by salih.sagdilek on 6.2.2015.
 */
'use strict';
module.exports = function(grunt){
    grunt.config('uglify', {
        build: {
	        options: {
		        sourceMap: true
	        },
	        files: {
		        'dist/flashMap.min.js': ['src/flashMap.js']
	        }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-uglify');
};
