module.exports = function( grunt ){
	"use strict";
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json')
	});
	grunt.loadTasks('build/tasks');
	grunt.registerTask('default', ['uglify', 'watch']);
};
