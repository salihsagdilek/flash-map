;(function ($, google, window, document, undefined) {
	'use strict';
	var pluginName     = 'flashMap',
	    defaultOptions = {
		    algorithm   : 'default',
		    zoom        : 2,
		    center      : {lat: 5.66, lng: 9.5},
		    items     : ['markers']
	    };

	var events     = [],
	    mapOptions = {};
	window.map = {};

	function FlashMap(element, options, name) {
		this.element = element;
		this.id = name || 'id' + new Date().getTime();

		if(options.constructor === Array) {
			this.data = {};
			this.data.markers = options;
		} else {
			this.data = options.data || options;
		}


		if (mapOptions[name]) {
			this.options = $.extend(true, {}, mapOptions[name], options);
		} else {
			this.options = $.extend(true, {}, defaultOptions, options);
		}
		if(options.data && options.data.center) {
			this.options.center = options.data.center
		}

		this.init();
	}

	FlashMap.prototype = {
		init: function () {
			google.maps.visualRefresh = true;
			var eventSize = events.length;
			map[this.id] = new google.maps.Map(this.element, this.options);

			if (this.options.styles) {
				map[this.id].setOptions({styles: this.options.styles});
			}

			if (events.length > 0) {
				var i          = 0,
				    name       = this.id,
				    currentMap = map[name],
				    event;

				for (i; i < eventSize; i++) {
					event = events[i];
					if (event.name === '*' || event.name === name) {
						google.maps.event.addListener(currentMap, event.type, function () {
							event.callback(currentMap)
						});
					}
				}
			}

			FlashMap.algorithm[this.options.algorithm].addMarkers(this);
		}
	};

	FlashMap.algorithm = {
		'default': {
			addMarkers: function (elem) {
				for(var j = 0; j < elem.options.items.length; j++) {
					var type       = elem.options.items[j],
					    items      = elem.data[type],
					    count      = items.length,
					    i          = 0,
					    infowindow = new google.maps.InfoWindow(),
					    bounds     = new google.maps.LatLngBounds(),
					    marker, pos;

					for (i; i < count; i++) {
						if (items[i].lat !== undefined && items[i].lng !== undefined) {
							pos = new google.maps.LatLng(items[i].lat, items[i].lng);
							bounds.extend(pos);
							marker = new google.maps.Marker({
								position: pos,
								map     : map[elem.id]
							});

							google.maps.event.addListener(marker, 'click', (function (marker, i) {
								return function () {
									infowindow.setContent(items[i].content);
									infowindow.open(map[elem.id], marker);
								}
							})(marker, i));
						}
					}
				}
			}
		},
		'flash'  : {
			addMarkers: function (elem) {
				for(var j = 0; j < elem.options.items.length; j++){
					var type          = elem.options.items[j],
						items         = elem.data[type],
					    itemCount     = items.length,
					    i             = 0,
					    latlng, marker;

					for (i; i < itemCount; i++) {
						if (items[i].lat !== undefined && items[i].lng !== undefined) {
							latlng = new google.maps.LatLng(items[i].lat, items[i].lng);
							marker = new FlashMarker(latlng, map[elem.id], type, items[i]);
						}
					}
				}
			}
		}
	};

	FlashMap.helpers = {
		set: function (name, options) {
			if (typeof name === 'object') {
				$.extend(true, defaultOptions, name);
			} else if (name === '*') {
				$.extend(true, defaultOptions, options);
			} else {
				mapOptions[name] = $.extend(true, {}, defaultOptions, options);
			}
		},
		get: function () {
			return defaultOptions;
		},
		Add: {
			event: function (type, name, callback) {
				if (typeof name === 'function') {
					callback = name;
					name = '*';
				}
				var event = {
					type    : type,
					name    : name,
					callback: callback
				};
				events.push(event);
			}
		}
	};
	FlashMap.manager = {
		events : {
			add : function(events, elem, root){
				if (typeof events === 'object') {
					for(var eventName in events) {
						google.maps.event.addDomListener(elem, eventName, function (e) {
							events[eventName](e, root.args);
							google.maps.event.trigger(root, eventName);
						});
					}
				}
			}
		}
	};

	/**
	 * ### Flash Marker
	 */
	function FlashMarker(latlng, map, type, args) {
		this.latlng = latlng;
		this.type = type;
		this.args = args;
		this.setMap(map);
	}

	FlashMarker.prototype = new google.maps.OverlayView();

	FlashMarker.prototype.draw = function () {
		var self = this;
		var div = this.div;
		if(typeof  this.map.createMarker === 'function'){
			if(!div) {
				div = this.div = this.map.createMarker(this.args, this.type);
				if(typeof div.get === 'function'){
					div = div[0];
				}
				if(div.style.position !== 'absolute') {
					div.style.position = 'absolute';
				}
				if(div.style.cursor === ''){
					div.style.cursor = 'pointer';
				}
				FlashMap.manager.events.add(this.map.events, div, self);
			}
		} else {
			if (!div) {
				div = this.div = document.createElement('div');
				div.className = 'marker';
				div.style.position = 'absolute';
				div.style.cursor = 'pointer';
				div.style.width = '20px';
				div.style.height = '20px';
				div.style.background = 'blue';

				if (typeof(self.args.marker_id) !== 'undefined') {
					div.dataset.marker_id = self.args.marker_id;
				}

				FlashMap.manager.events.add(this.map.events, div, self);
			}
		}
		var panes = this.getPanes();
		if(typeof div.get === 'function'){
			div = div[0];
		}
		panes.overlayImage.appendChild(div);
		var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

		if (point) {
			div.style.left = (point.x - 10) + 'px';
			div.style.top = (point.y - 20) + 'px';
		}
	};

	FlashMarker.prototype.remove = function () {
		if (this.div) {
			this.div.parentNode.removeChild(this.div);
			this.div = null;
		}
	};

	FlashMarker.prototype.getPosition = function () {
		return this.latlng;
	};

	$.fn[pluginName] = function (options, name) {
		return this.each(function () {
			$.data(this, pluginName, new FlashMap(this, options, name));
		});
	};

	$[pluginName] = FlashMap.helpers;
})(jQuery, google, window, document);
