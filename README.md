#Flash Map - the fastest map alive
##2,50 kb light size, easy to use, highly configurable jQuery library


##example data
    :::javascript
        var data = [
                {
                    content: 'Bondi Beach',
                    lat    : -33.890542,
                    lng    : 151.274856
                },
                {
                    content: 'Coogee Beach',
                    lat    : -33.923036,
                    lng    : 151.259052
                },
                {
                    content: 'Cronulla Beach',
                    lat    : -34.028249,
                    lng    : 151.157507
                },
                {
                    content: 'Manly Beach',
                    lat    : -33.80010128657071,
                    lng    : 151.28747820854187
                }
            ];

#new basic usage
    :::javascript
    $('#map2').flashMap(data);
##basic usage
    :::javascript
    $('#map').flashMap({
            center: {lat: -33.890542, lng: 151.274856},
            zoom  : 10
        }, data)

## set map name
    :::javascript
    $('#map').flashMap({
            center: {lat: -33.890542, lng: 151.274856},
            zoom  : 10
        }, data, 'salih')

## add event
### wildcard
    :::javascript
    $.flashMap.Add.event('zoom_changed', function (map) {
        var zoomLevel = map.getZoom();
        if (zoomLevel > 17) {
            map.setMapTypeId(google.maps.MapTypeId.HYBRID);
        } else {
            map.setMapTypeId(google.maps.MapTypeId.ROADMAP)
        }
    });
####or
    :::javascript
    $.flashMap.Add.event('zoom_changed', '*', function (map) {
                var zoomLevel = map.getZoom();
                if (zoomLevel > 17) {
                    map.setMapTypeId(google.maps.MapTypeId.HYBRID);
                } else {
                    map.setMapTypeId(google.maps.MapTypeId.ROADMAP)
                }
            });
###named
    :::javascript
    $.flashMap.Add.event('zoom_changed', 'mapname', function (map) {
                var zoomLevel = map.getZoom();
                if (zoomLevel > 17) {
                    map.setMapTypeId(google.maps.MapTypeId.HYBRID);
                } else {
                    map.setMapTypeId(google.maps.MapTypeId.ROADMAP)
                }
            });
## set configs
### wildcard
    :::javascript
     $.flashMap.set({
            zoomControl          : true,
            zoomControlOptions   : {
                style   : google.maps.ZoomControlStyle.SMALL,
                position: google.maps.ControlPosition.TOP_RIGHT
            },
            panControl           : false,
            panControlOptions    : {
                position: google.maps.ControlPosition.TOP_RIGHT
            },
            mapTypeControl       : true,
            mapTypeId            : google.maps.MapTypeId.ROADMAP,
            heading              : 90,
            tilt                 : 45,
            mapTypeControlOptions: {
                style   : google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.TOP_CENTER
            },
            styles               : [
                {
                    "featureType": "transit",
                    "stylers"    : [
                        {"visibility": "off"}
                    ]
                }, {
                    "featureType": "poi.school",
                    "stylers"    : [
                        {"visibility": "off"}
                    ]
                }, {
                    "featureType": "poi.medical",
                    "stylers"    : [
                        {"visibility": "off"}
                    ]
                }, {
                    "featureType": "poi.sports_complex",
                    "stylers"    : [
                        {"visibility": "off"}
                    ]
                }
            ]
        });
####or
    :::javascript
    $.flashMap.set('*', {
                zoomControl          : true,
                zoomControlOptions   : {
                    style   : google.maps.ZoomControlStyle.SMALL,
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                panControl           : false,
                panControlOptions    : {
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                mapTypeControl       : true,
                mapTypeId            : google.maps.MapTypeId.ROADMAP,
                heading              : 90,
                tilt                 : 45,
                mapTypeControlOptions: {
                    style   : google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                    position: google.maps.ControlPosition.TOP_CENTER
                },
                styles               : [
                    {
                        "featureType": "transit",
                        "stylers"    : [
                            {"visibility": "off"}
                        ]
                    }, {
                        "featureType": "poi.school",
                        "stylers"    : [
                            {"visibility": "off"}
                        ]
                    }, {
                        "featureType": "poi.medical",
                        "stylers"    : [
                            {"visibility": "off"}
                        ]
                    }, {
                        "featureType": "poi.sports_complex",
                        "stylers"    : [
                            {"visibility": "off"}
                        ]
                    }
                ]
            });

###named
    :::javascript
    $.flashMap.set('mapName', {
                    zoomControl          : true,
                    zoomControlOptions   : {
                        style   : google.maps.ZoomControlStyle.SMALL,
                        position: google.maps.ControlPosition.TOP_RIGHT
                    },
                    panControl           : false,
                    panControlOptions    : {
                        position: google.maps.ControlPosition.TOP_RIGHT
                    },
                    mapTypeControl       : true,
                    mapTypeId            : google.maps.MapTypeId.ROADMAP,
                    heading              : 90,
                    tilt                 : 45,
                    mapTypeControlOptions: {
                        style   : google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                        position: google.maps.ControlPosition.TOP_CENTER
                    },
                    styles               : [
                        {
                            "featureType": "transit",
                            "stylers"    : [
                                {"visibility": "off"}
                            ]
                        }, {
                            "featureType": "poi.school",
                            "stylers"    : [
                                {"visibility": "off"}
                            ]
                        }, {
                            "featureType": "poi.medical",
                            "stylers"    : [
                                {"visibility": "off"}
                            ]
                        }, {
                            "featureType": "poi.sports_complex",
                            "stylers"    : [
                                {"visibility": "off"}
                            ]
                        }
                    ]
                });
###Access the named map
    :::javascript
    map[name]
####example
    :::javascript
        $('#map').flashMap({
                center: {lat: -33.890542, lng: 151.274856},
                zoom  : 10
            }, data, 'salih')

    map.salih;
    //or
    map['salih']

#Flash Map - the fastest map alive

##forget the canvas, think like html
you can compare 4000 markers with the dafult algorithm if you want to.

###example data
    :::javascript
        var data = {
            "landmarkList": [
                "6",
                "1",
                "9",
                "3",
                "11"
            ],
            "landmarks": [
                {
                    "ID": "#0",
                    "lat": 43.6794,
                    "lng": 10.3464,
                    "name": "Basilica of San Piero a Grado",
                    "type": "Landmark"
                },
                {
                    "ID": "#6",
                    "lat": 43.716135,
                    "lng": 10.3966024,
                    "name": "City Center",
                    "type": "City Center"
                },
                {
                    "ID": "#1",
                    "lat": 43.6839,
                    "lng": 10.3925,
                    "name": "Galileo Galilei (PSA)",
                    "type": "Airports"
                },
                {
                    "ID": "#0",
                    "lat": 43.72,
                    "lng": 10.3961,
                    "name": "Giardino botanico",
                    "type": "Landmark"
                },
                {
                    "ID": "#9",
                    "lat": 43.6331,
                    "lng": 10.3016,
                    "name": "Golf & Country Club Cosmopolitan",
                    "type": "Golf Course"
                },
                {
                    "ID": "#9",
                    "lat": 43.6242,
                    "lng": 10.2943,
                    "name": "Golf Club Tirrenia",
                    "type": "Golf Course"
                },
                {
                    "ID": "#0",
                    "lat": 43.71,
                    "lng": 10.3987,
                    "name": "ILM Pisa",
                    "type": "Business Area"
                },
                {
                    "ID": "#0",
                    "lat": 43.7243,
                    "lng": 10.3955,
                    "name": "Leaning Tower of Pisa",
                    "type": "Monument Or Landmark"
                },
                {
                    "ID": "#0",
                    "lat": 43.7233,
                    "lng": 10.3953,
                    "name": "Pisa Cathedral",
                    "type": "Monument Or Landmark"
                },
                {
                    "ID": "#3",
                    "lat": 43.7083,
                    "lng": 10.3981,
                    "name": "Pisa Centrale railway station",
                    "type": "Train Station"
                },
                {
                    "ID": "#0",
                    "lat": 43.7245,
                    "lng": 10.3992,
                    "name": "Pisa Stadium Romeo Anconetani",
                    "type": "Stadium or Arena"
                },
                {
                    "ID": "#0",
                    "lat": 43.6277259,
                    "lng": 10.2917118,
                    "name": "Tirrenia",
                    "type": "Districts"
                },
                {
                    "ID": "#11",
                    "lat": 43.7093,
                    "lng": 10.3843,
                    "name": "Via Arrosti",
                    "type": "Popular Area"
                }
            ],
            "lat": 43.716135,
            "items": [
                {
                    "ID": 115030,
                    "data": [
                        "4",
                        " L Hotel Di Pisa",
                        "\/0\/3\/0\/5\/1\/1\/0\/115030\/T\/1.jpg",
                        "Via Statale Del Brennero, 13 56017 (ex. Easy Hotel) (Ex.Granduca Tuscany)\u000d\u000a\u000d\u000a",
                        "0.00",
                        "115030",
                        "green",
                        ""
                    ],
                    "lat": 43.7607114106644,
                    "lng": 10.4408311843872,
                    "price": 0
                },
                {
                    "ID": 2171576,
                    "data": [
                        "0",
                        "A Casa Doina Airport",
                        "\/6\/7\/5\/1\/7\/1\/2\/2171576\/T\/1.jpg",
                        "Via San Giusto 4",
                        "0.00",
                        "2171576",
                        "green",
                        ""
                    ],
                    "lat": 43.69728,
                    "lng": 10.39426,
                    "price": 0
                },
                {
                    "ID": 796914,
                    "data": [
                        "5",
                        "Abitalia Tower Plaza Hotel",
                        "\/4\/1\/9\/6\/9\/7\/0\/796914\/T\/10.jpg",
                        "Via Caduti Del Lavoro 46 56122",
                        "0.00",
                        "796914",
                        "green",
                        ""
                    ],
                    "lat": 43.720629,
                    "lng": 10.377794,
                    "price": 0
                },
                {
                    "ID": 224826,
                    "data": [
                        "4",
                        "Ac Hotel Pisa By Marriott",
                        "\/6\/2\/8\/4\/2\/2\/0\/224826\/T\/23.jpg",
                        "Via delle Torri, 20 - 56124\u000d\u000a",
                        "0.00",
                        "224826",
                        "green",
                        ""
                    ],
                    "lat": 43.7075,
                    "lng": 10.4325,
                    "price": 0
                },
                {
                    "ID": 224827,
                    "data": [
                        "4",
                        "Accademia Palace",
                        "\/7\/2\/8\/4\/2\/2\/0\/224827\/T\/1.jpg",
                        "Viale Giovanni Gronchi - 56121",
                        "0.00",
                        "224827",
                        "green",
                        ""
                    ],
                    "lat": 43.679765,
                    "lng": 10.427851,
                    "price": 0
                },
                {
                    "ID": 1876883,
                    "data": [
                        "3",
                        "Agrihotel Il Palagetto",
                        "\/3\/8\/8\/6\/7\/8\/1\/1876883\/T\/13.jpg",
                        "Podere Il Palagetto 41 - Loc. Cozzano 56048 Volterra Italy",
                        "0.00",
                        "1876883",
                        "green",
                        ""
                    ],
                    "lat": 43.453067,
                    "lng": 10.877773,
                    "price": 0
                },
                {
                    "ID": 35934,
                    "data": [
                        "2",
                        "Agriturismo Green Farm",
                        "\/4\/3\/9\/5\/3\/0\/0\/35934\/T\/3.jpg",
                        "Via Vecchia Piestrasantina, 11 56010 ",
                        "0.00",
                        "35934",
                        "green",
                        ""
                    ],
                    "lat": 43.7533031591264,
                    "lng": 10.3501081466674,
                    "price": 0
                },
                {
                    "ID": 139209,
                    "data": [
                        "2",
                        "Airone Pisa Park",
                        "\/9\/0\/2\/9\/3\/1\/0\/139209\/T\/16.jpg",
                        "Via Santa Elena  4",
                        "0.00",
                        "139209",
                        "green",
                        ""
                    ],
                    "lat": 43.73743,
                    "lng": 10.42979,
                    "price": 0
                },
                {
                    "ID": 141801,
                    "data": [
                        "3",
                        "Albergo Nazionale",
                        "\/1\/0\/8\/1\/4\/1\/0\/141801\/T\/26.jpg",
                        "Via Dei Marchesi 11 56048",
                        "0.00",
                        "141801",
                        "green",
                        ""
                    ],
                    "lat": 43.4019295471977,
                    "lng": 10.8597278594971,
                    "price": 0
                },
                {
                    "ID": 1902249,
                    "data": [
                        "3",
                        "Albergo Nazionale",
                        "\/9\/4\/2\/2\/0\/9\/1\/1902249\/T\/1.jpg",
                        "Via Dei Marchesi 11",
                        "0.00",
                        "1902249",
                        "green",
                        ""
                    ],
                    "lat": 43.40137,
                    "lng": 10.859794,
                    "price": 0
                },
                {
                    "ID": 139200,
                    "data": [
                        "5",
                        "Alessandro Della Spina",
                        "\/0\/0\/2\/9\/3\/1\/0\/139200\/T\/17.jpg",
                        "Via Alessandro Della Spina 2 \/ 7 \/ 9 56125",
                        "0.00",
                        "139200",
                        "green",
                        ""
                    ],
                    "lat": 43.70973,
                    "lng": 10.40305,
                    "price": 0
                },
                {
                    "ID": 1901267,
                    "data": [
                        "3",
                        "Antico Borgo San Martino",
                        "\/7\/6\/2\/1\/0\/9\/1\/1901267\/T\/1.jpg",
                        "San Martino  Riparbella Province Of Pisa",
                        "0.00",
                        "1901267",
                        "green",
                        ""
                    ],
                    "lat": 43.341808,
                    "lng": 10.5875,
                    "price": 0
                }
            ],
            "lng": 10.3966024,
            "regions": [
                {
                    "C": [
                        {
                            "O": 10.379076,
                            "S": 1,
                            "T": 43.723536
                        },
                        {
                            "O": 10.372424,
                            "S": 2,
                            "T": 43.72273
                        },
                        {
                            "O": 10.369635,
                            "S": 3,
                            "T": 43.72242
                        },
                        {
                            "O": 10.360537,
                            "S": 4,
                            "T": 43.721179
                        },
                        {
                            "O": 10.363026,
                            "S": 5,
                            "T": 43.713921
                        },
                        {
                            "O": 10.364356,
                            "S": 6,
                            "T": 43.710943
                        },
                        {
                            "O": 10.364184,
                            "S": 7,
                            "T": 43.70902
                        },
                        {
                            "O": 10.363584,
                            "S": 8,
                            "T": 43.707779
                        },
                        {
                            "O": 10.364227,
                            "S": 9,
                            "T": 43.705701
                        },
                        {
                            "O": 10.364313,
                            "S": 10,
                            "T": 43.703653
                        },
                        {
                            "O": 10.367661,
                            "S": 11,
                            "T": 43.705173
                        },
                        {
                            "O": 10.37281,
                            "S": 12,
                            "T": 43.707686
                        },
                        {
                            "O": 10.375729,
                            "S": 13,
                            "T": 43.709082
                        },
                        {
                            "O": 10.379634,
                            "S": 14,
                            "T": 43.710416
                        },
                        {
                            "O": 10.381694,
                            "S": 15,
                            "T": 43.710912
                        },
                        {
                            "O": 10.384483,
                            "S": 16,
                            "T": 43.711564
                        },
                        {
                            "O": 10.379076,
                            "S": 17,
                            "T": 43.723536
                        }
                    ],
                    "I": 1445,
                    "N": "Barbaricina"
                },
                {
                    "C": [
                        {
                            "O": 10.384741,
                            "S": 1,
                            "T": 43.711688
                        },
                        {
                            "O": 10.391521,
                            "S": 2,
                            "T": 43.713611
                        },
                        {
                            "O": 10.395126,
                            "S": 3,
                            "T": 43.715472
                        },
                        {
                            "O": 10.399418,
                            "S": 4,
                            "T": 43.716527
                        },
                        {
                            "O": 10.403194,
                            "S": 5,
                            "T": 43.71572
                        },
                        {
                            "O": 10.406027,
                            "S": 6,
                            "T": 43.714666
                        },
                        {
                            "O": 10.408773,
                            "S": 7,
                            "T": 43.713673
                        },
                        {
                            "O": 10.40916,
                            "S": 8,
                            "T": 43.714728
                        },
                        {
                            "O": 10.413837,
                            "S": 9,
                            "T": 43.71572
                        },
                        {
                            "O": 10.415082,
                            "S": 10,
                            "T": 43.716031
                        },
                        {
                            "O": 10.418944,
                            "S": 11,
                            "T": 43.718419
                        },
                        {
                            "O": 10.420275,
                            "S": 12,
                            "T": 43.719318
                        },
                        {
                            "O": 10.422249,
                            "S": 13,
                            "T": 43.720714
                        },
                        {
                            "O": 10.415812,
                            "S": 14,
                            "T": 43.722978
                        },
                        {
                            "O": 10.40946,
                            "S": 15,
                            "T": 43.724715
                        },
                        {
                            "O": 10.401735,
                            "S": 16,
                            "T": 43.723226
                        },
                        {
                            "O": 10.392637,
                            "S": 17,
                            "T": 43.725149
                        },
                        {
                            "O": 10.386286,
                            "S": 18,
                            "T": 43.724715
                        },
                        {
                            "O": 10.379248,
                            "S": 19,
                            "T": 43.723909
                        },
                        {
                            "O": 10.384741,
                            "S": 20,
                            "T": 43.711688
                        }
                    ],
                    "I": 1446,
                    "N": "Pisa City Center"
                },
                {
                    "C": [
                        {
                            "O": 10.388603,
                            "S": 1,
                            "T": 43.711502
                        },
                        {
                            "O": 10.384912,
                            "S": 2,
                            "T": 43.710602
                        },
                        {
                            "O": 10.383153,
                            "S": 3,
                            "T": 43.710106
                        },
                        {
                            "O": 10.381522,
                            "S": 4,
                            "T": 43.709982
                        },
                        {
                            "O": 10.381007,
                            "S": 5,
                            "T": 43.7073759999999
                        },
                        {
                            "O": 10.38075,
                            "S": 6,
                            "T": 43.701171
                        },
                        {
                            "O": 10.380707,
                            "S": 7,
                            "T": 43.700892
                        },
                        {
                            "O": 10.382295,
                            "S": 8,
                            "T": 43.700737
                        },
                        {
                            "O": 10.384998,
                            "S": 9,
                            "T": 43.700799
                        },
                        {
                            "O": 10.387402,
                            "S": 10,
                            "T": 43.700178
                        },
                        {
                            "O": 10.389161,
                            "S": 11,
                            "T": 43.699682
                        },
                        {
                            "O": 10.391564,
                            "S": 12,
                            "T": 43.699434
                        },
                        {
                            "O": 10.39547,
                            "S": 13,
                            "T": 43.699868
                        },
                        {
                            "O": 10.397573,
                            "S": 14,
                            "T": 43.69993
                        },
                        {
                            "O": 10.400534,
                            "S": 15,
                            "T": 43.699682
                        },
                        {
                            "O": 10.404439,
                            "S": 16,
                            "T": 43.698999
                        },
                        {
                            "O": 10.407486,
                            "S": 17,
                            "T": 43.698193
                        },
                        {
                            "O": 10.41049,
                            "S": 18,
                            "T": 43.695834
                        },
                        {
                            "O": 10.412507,
                            "S": 19,
                            "T": 43.6945
                        },
                        {
                            "O": 10.413923,
                            "S": 20,
                            "T": 43.693786
                        },
                        {
                            "O": 10.41388,
                            "S": 21,
                            "T": 43.695152
                        },
                        {
                            "O": 10.414481,
                            "S": 22,
                            "T": 43.696579
                        },
                        {
                            "O": 10.41328,
                            "S": 23,
                            "T": 43.700054
                        },
                        {
                            "O": 10.412335,
                            "S": 24,
                            "T": 43.700923
                        },
                        {
                            "O": 10.412207,
                            "S": 25,
                            "T": 43.702319
                        },
                        {
                            "O": 10.407228,
                            "S": 26,
                            "T": 43.704615
                        },
                        {
                            "O": 10.403109,
                            "S": 27,
                            "T": 43.706817
                        },
                        {
                            "O": 10.401778,
                            "S": 28,
                            "T": 43.7075
                        },
                        {
                            "O": 10.39607,
                            "S": 29,
                            "T": 43.708679
                        },
                        {
                            "O": 10.394697,
                            "S": 30,
                            "T": 43.708617
                        },
                        {
                            "O": 10.390105,
                            "S": 31,
                            "T": 43.710168
                        },
                        {
                            "O": 10.388603,
                            "S": 32,
                            "T": 43.711502
                        }
                    ],
                    "I": 1447,
                    "N": "San Giusto"
                },
                {
                    "C": [
                        {
                            "O": 10.389333,
                            "S": 1,
                            "T": 43.711719
                        },
                        {
                            "O": 10.390277,
                            "S": 2,
                            "T": 43.710323
                        },
                        {
                            "O": 10.391779,
                            "S": 3,
                            "T": 43.709516
                        },
                        {
                            "O": 10.394139,
                            "S": 4,
                            "T": 43.708927
                        },
                        {
                            "O": 10.394697,
                            "S": 5,
                            "T": 43.708772
                        },
                        {
                            "O": 10.395813,
                            "S": 6,
                            "T": 43.708896
                        },
                        {
                            "O": 10.398946,
                            "S": 7,
                            "T": 43.708151
                        },
                        {
                            "O": 10.400448,
                            "S": 8,
                            "T": 43.707872
                        },
                        {
                            "O": 10.402164,
                            "S": 9,
                            "T": 43.707531
                        },
                        {
                            "O": 10.403624,
                            "S": 10,
                            "T": 43.706724
                        },
                        {
                            "O": 10.404396,
                            "S": 11,
                            "T": 43.706414
                        },
                        {
                            "O": 10.405726,
                            "S": 12,
                            "T": 43.705701
                        },
                        {
                            "O": 10.407572,
                            "S": 13,
                            "T": 43.704553
                        },
                        {
                            "O": 10.40916,
                            "S": 14,
                            "T": 43.703808
                        },
                        {
                            "O": 10.412378,
                            "S": 15,
                            "T": 43.702474
                        },
                        {
                            "O": 10.415082,
                            "S": 16,
                            "T": 43.70114
                        },
                        {
                            "O": 10.415039,
                            "S": 17,
                            "T": 43.704708
                        },
                        {
                            "O": 10.41019,
                            "S": 18,
                            "T": 43.706662
                        },
                        {
                            "O": 10.408602,
                            "S": 19,
                            "T": 43.707748
                        },
                        {
                            "O": 10.408001,
                            "S": 20,
                            "T": 43.709144
                        },
                        {
                            "O": 10.408087,
                            "S": 21,
                            "T": 43.710695
                        },
                        {
                            "O": 10.408173,
                            "S": 22,
                            "T": 43.712929
                        },
                        {
                            "O": 10.406585,
                            "S": 23,
                            "T": 43.713704
                        },
                        {
                            "O": 10.403409,
                            "S": 24,
                            "T": 43.7151
                        },
                        {
                            "O": 10.400362,
                            "S": 25,
                            "T": 43.715938
                        },
                        {
                            "O": 10.397615,
                            "S": 26,
                            "T": 43.715627
                        },
                        {
                            "O": 10.395384,
                            "S": 27,
                            "T": 43.71479
                        },
                        {
                            "O": 10.389333,
                            "S": 28,
                            "T": 43.711719
                        }
                    ],
                    "I": 1448,
                    "N": "San Martino"
                }
            ]
        };
### Basic usage
    :::javascript
        $('#map').flashMap({
            algorithm: 'flash',
            center: {lat: 43.716135, lng: 10.3966024},
            zoom  : 11
        }, data, 'salih');


## You can also use google maps methods
###basic
    :::javascript
    map.salih.setOptions({disableDefaultUI: true})

    map.salih.setOptions({center: new google.maps.LatLng(-33, 151),})

    map.salih.getMapTypeId()

    map.salih.getZoom()
### click to add marker example
    :::javascipt
    google.maps.event.addListener(map.salih, 'click', function(e) {
        placeMarker(e.latLng, map.salih);
      });

    function placeMarker(position, map) {
      var marker = new google.maps.Marker({
        position: position,
        map: map
      });
      map.panTo(position);
    }
# new usage
    :::javascript
    $('#map').flashMap({
            algorithm: 'flash',
            data    : data2,
            zoom  : 11,
            createMarker : function(data){
               return $('<div>', {
                   id: data.ID,
                   css : {
                       width : '10px',
                       height: '10px',
                       backgroundColor: 'red'
                   }
               });
            },
            events : {
                click : function(event, data){
                    alert(data.ID);
                },
                mouseover : function(event, data){
                    alert(data.ID);
                }
            }
        }, 'salih');
